
db.users.insertMany([
		{
			"firstName" : "Patrick",
			"lastName" : "Stump",
			"email" : "ps@mail.com",
			"password" : "pass1",
			"isAdmin" : false,
		},
		{
			"firstName" : "Joe",
			"lastName" : "Trohman",
			"email" : "jt@mail.com",
			"password" : "pass2",
			"isAdmin" : false,
		},
		{
			"firstName" : "Pete",
			"lastName" : "Wentz",
			"email" : "pw@mail.com",
			"password" : "pass3",
			"isAdmin" : false,
		},
		{
			"firstName" : "Andy",
			"lastName" : "Hurley",
			"email" : "ah@mail.com",
			"password" : "pass4",
			"isAdmin" : false,
		},
		{
			"firstName" : "Ben",
			"lastName" : "Rose",
			"email" : "br@mail.com",
			"password" : "pass5",
			"isAdmin" : false,
		}

	]);
db.course.insertMany([
		{
			"name" : "Guitar Lesson",
			"price" : 5000,
			"isActive" : false,
		},
		{
			"name" : "Drum Lesson",
			"price" : 10000,
			"isActive" : false,
		},
		{
			"name" : "Voice Lesson",
			"price" : 12000,
			"isActive" : false,
		}
		
	])

db.users.find({"isAdmin" : false})

db.users.updateOne({},{$set : {"isAdmin" : true}})
db.course.updateOne({"name" : "Voice Lesson"}, {$set :{"isActive" : true}})

db.course.deleteMany({"isActive" : false})
